﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Core.Encoding
{
    public static class HabboEncoding
    {
        #region Base64
        public static string encodeB64(int value, int length)
        {
            string stack = "";
            for (int x = 1; x <= length; x++)
            {
                int offset = 6 * (length - x);
                byte val = (byte)(64 + (value >> offset & 0x3f));
                stack += (char)val;
            }
            return stack;
        }

        public static int decodeB64(string Val)
        {
            char[] val = Val.ToCharArray();
            int intTot = 0;
            int y = 0;
            for (int x = (val.Length - 1); x >= 0; x--)
            {
                int intTmp = (int)(byte)((val[x] - 64));
                if (y > 0)
                {
                    intTmp = intTmp * (int)(Math.Pow(64, y));
                }
                intTot += intTmp;
                y++;
            }

            return intTot;
        }
        #endregion

        #region VL64
        public static string encodeVL64(int i)
        {
            byte[] wf = new byte[6];
            int pos = 0;
            int startPos = pos;
            int bytes = 1;
            int negativeMask = i >= 0 ? 0 : 4;
            i = Math.Abs(i);
            wf[pos++] = (byte)(64 + (i & 3));
            for (i >>= 2; i != 0; i >>= 6)
            {
                bytes++;
                wf[pos++] = (byte)(64 + (i & 0x3f));
            }

            wf[startPos] = (byte)(wf[startPos] | bytes << 3 | negativeMask);


            StringBuilder sb = new StringBuilder();
            for (byte b = 0; b < 6; b++)
            {
                if (wf[b] != 0)
                {
                    sb.Append((char)wf[b]);
                }
            }
            return sb.ToString();
        }
        public static int decodeVL64(string data)
        {
            return decodeVL64(data.ToCharArray());
        }
        public static int decodeVL64(char[] raw)
        {
            try
            {
                int pos = 0;
                int v = 0;
                bool negative = (raw[pos] & 4) == 4;
                int totalBytes = raw[pos] >> 3 & 7;
                v = raw[pos] & 3;
                pos++;
                int shiftAmount = 2;
                for (int b = 1; b < totalBytes; b++)
                {
                    v |= (raw[pos] & 0x3f) << shiftAmount;
                    shiftAmount = 2 + 6 * b;
                    pos++;
                }

                if (negative)
                    v *= -1;

                return v;
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region B64 Parameters Functions

        public static string getParameter(string s, int parameterLocation)
        {
            try
            {
                int j = 0;
                while (s.Length > 0)
                {
                    int i = decodeB64(s.Substring(0, 2));
                    if (j == parameterLocation)
                        return s.Substring(2, i);

                    s = s.Substring(i + 2);
                    j++;
                }
            }
            catch { }

            return "";
        }
        /// <summary>
        /// Searches through a given string for a parameter with a ID and returns it. If not found, then "" is returned.
        /// </summary>
        /// <param name="messageContent">The string to search through.</param>
        /// <param name="paramID">The ID of the parameter to get.</param>
        public static string getStructuredParameter(string s, int paramID)
        {
            try
            {
                int Cycles = 0;
                float maxCyles = s.Length / 4;
                while (Cycles <= maxCyles)
                {
                    int cID = decodeB64(s.Substring(0, 2));
                    int cLength = decodeB64(s.Substring(2, 2));
                    if (cID == paramID)
                        return s.Substring(4, cLength);

                    s = s.Substring(cLength + 4);
                }
            }
            catch { }

            return "";
        }
        /// <summary>
        /// Gets all parameters from a string encoded with Base64 headers, and returns it as a string array.
        /// </summary>
        /// <param name="messageContent">The content to get the parameters off.</param>
        public static string[] getParameters(string messageContent)
        {
            List<string> res = new List<string>();
            try
            {
                while (messageContent.Length > 0)
                {
                    int v = decodeB64(messageContent.Substring(0, 2));
                    res.Add(messageContent.Substring(2, v));
                    messageContent = messageContent.Substring(2 + v);
                }
            }
            catch { }

            return res.ToArray();
        }

        #endregion

        #region wireEncoding

        public static int WireDecode(ref string s)
        {
            try
            {
                char[] raw = s.ToCharArray();
                int pos = 0;
                int i = 0;
                bool negative = (raw[pos] & 4) == 4;
                int totalBytes = raw[pos] >> 3 & 7;
                i = raw[pos] & 3;
                pos++;
                int shiftAmount = 2;
                for (int b = 1; b < totalBytes; b++)
                {
                    i |= (raw[pos] & 0x3f) << shiftAmount;
                    shiftAmount = 2 + 6 * b;
                    pos++;
                }

                if (negative == true)
                    i *= -1;
                return i;
            }
            catch { return 0; }
        }

        public static int WireDecode(string s)
        {
            return WireDecode(ref s);
        }

        public static int[] decodeWire(string s)
        {
            List<int> Ret = new List<int>();
            try
            {
                while (s.Length > 0)
                {
                    int i = WireDecode(ref s);
                    Ret.Add(i);
                    s = s.Substring(encodeVL64(i).Length);
                }
            }
            catch { }

            return Ret.ToArray();
        }

        #endregion

        #region Mixed Encoding

        public static string[] getMixedParameters(string s)
        {
            List<string> Ret = new List<string>();
            while (s.Length > 0)
            {
                int len = 0;
                string sVar = "";

                if ((int)s[0] < 72) // Base64
                {
                    len = decodeB64(s.Substring(0, 2));
                    sVar = s.Substring(2, len);
                    len += 2;
                }
                else
                {
                    int w = WireDecode(ref s);
                    sVar = w.ToString();
                    len = encodeVL64(w).Length;
                }
                s = s.Substring(len);
                Ret.Add(sVar);
            }

            return Ret.ToArray();
        }
        public static int[] getMixedParametersAsIntegers(string s)
        {
            List<int> ret = new List<int>();
            while (s.Length > 0)
            {
                int len = 0;
                int iVar = 0;
                if ((int)s[0] < 72) // Base64
                {
                    len = decodeB64(s.Substring(0, 2));
                    iVar = int.Parse(s.Substring(2, len));
                    len += 2;
                }
                else
                {
                    int w = WireDecode(ref s);
                    iVar = int.Parse(w.ToString());
                    len = encodeVL64(w).Length;
                }
                s = s.Substring(len);
                ret.Add(iVar);
            }

            return ret.ToArray();
        }

        #endregion
    }
}
