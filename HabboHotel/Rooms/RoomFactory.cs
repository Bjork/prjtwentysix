﻿using PrjTwentySix.Communication.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Rooms
{
    public class RoomFactory
    {
        public static bool TryGetData(int roomId, out RoomData data)
        {
            Room room = null;
            if (PrjTwentySixEnv.GetGame().GetRoomManager().TryGetRoom(roomId, out room))
            {
                data = room;
                return true;
            }

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `rooms` WHERE `id` = @id LIMIT 1;");
                dbClient.AddParameter("id", roomId);
                DataRow dRow = dbClient.getRow();

                if (dRow != null)
                {
                    RoomModel model = null;
                    if (!PrjTwentySixEnv.GetGame().GetRoomManager().TryGetModel(Convert.ToString(dRow["model"]), out model))
                    {
                        data = null;
                        return false;
                    }

                    data = new RoomData(Convert.ToInt32(dRow["id"]), Convert.ToString(dRow["name"]), Convert.ToString(dRow["description"]), Convert.ToInt32(dRow["owner_id"]), 
                        Convert.ToInt32(dRow["category"]), Convert.ToString(dRow["model"]), Convert.ToString(dRow["ccts"]),
                        Convert.ToInt32(dRow["floor"]), Convert.ToInt32(dRow["wallpaper"]), Convert.ToString(dRow["landscape"]), Convert.ToString(dRow["state"]),
                        Convert.ToString(dRow["password"]), PrjTwentySixEnv.EnumToBool(dRow["showname"].ToString()), PrjTwentySixEnv.EnumToBool(dRow["superusers"].ToString()),
                        Convert.ToInt32(dRow["users_now"]), Convert.ToInt32(dRow["users_max"]));
                    return true;
                }
            }

            data = null;
            return false;
        }

        public static List<RoomData> TryGetDataByCategory(int categoryId)
        {
            List<RoomData> data = new List<RoomData>();

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `rooms` WHERE `category` = @category_id ORDER BY RAND() DESC LIMIT 80");
                dbClient.AddParameter("category_id", categoryId);
                DataTable Table = dbClient.getTable();

                if (Table != null)
                {
                    foreach (DataRow dRow in Table.Rows)
                    {
                        Room room = null;
                        if (PrjTwentySixEnv.GetGame().GetRoomManager().TryGetRoom(Convert.ToInt32(dRow["id"]), out room))
                        {
                            data.Add(room);
                        }
                        else
                        {
                            data.Add(new RoomData(Convert.ToInt32(dRow["id"]), Convert.ToString(dRow["name"]), Convert.ToString(dRow["description"]), Convert.ToInt32(dRow["owner_id"]),
                                Convert.ToInt32(dRow["category"]), Convert.ToString(dRow["model"]), Convert.ToString(dRow["ccts"]),
                                Convert.ToInt32(dRow["floor"]), Convert.ToInt32(dRow["wallpaper"]), Convert.ToString(dRow["landscape"]), Convert.ToString(dRow["state"]),
                                Convert.ToString(dRow["password"]), PrjTwentySixEnv.EnumToBool(dRow["showname"].ToString()), PrjTwentySixEnv.EnumToBool(dRow["superusers"].ToString()),
                                Convert.ToInt32(dRow["users_now"]), Convert.ToInt32(dRow["users_max"])));

                        }
                    }
                }
            }
            return data;
        }
    }
}
