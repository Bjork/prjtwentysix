﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Rooms
{
    public static class RoomAccessUtility
    {
        public static int GetRoomAccessPacketNum(RoomAccess Access)
        {
            switch (Access)
            {
                default:
                case RoomAccess.OPEN:
                    return 0;

                case RoomAccess.DOORBELL:
                    return 1;

                case RoomAccess.PASSWORD:
                    return 2;
            }
        }

        public static RoomAccess ToRoomAccess(string Id)
        {
            switch (Id)
            {
                default:
                case "open":
                    return RoomAccess.OPEN;

                case "locked":
                    return RoomAccess.DOORBELL;

                case "password":
                    return RoomAccess.PASSWORD;
            }
        }

        public static string ToRoomAccessString(int Id)
        {
            switch (Id)
            {
                default:
                case 0:
                    return "open";

                case 1:
                    return "close";

                case 2:
                    return "password";
            }
        }
    }
}
