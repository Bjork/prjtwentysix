﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Rooms
{
    public class RoomData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }
        public string Owner { get; set; }
        public int CategoryId { get; set; }
        public string ModelName { get; set; }
        public string CCTS { get; set; }
        public int Floor { get; set; }
        public int Wallpaper { get; set; }
        public string Landscape { get; set; }
        public RoomAccess State { get; set; }
        public string Password { get; set; }
        public bool ShowName { get; set; }
        public bool SuperUsers { get; set; }
        public int UsersNow { get; set; }
        public int UsersMax { get; set; }

        private RoomModel mModel;

        public RoomData(int Id, string Caption, string Description, int OwnerId, int CategoryId, string ModelName, string CCTS, int Floor, int Wallpaper, string Landscape, string State, string Password, bool ShowName, bool SuperUsers, int UsersNow, int UsersMax)
        {
            this.Id = Id;
            this.Name = Caption;
            this.Description = Description;
            this.OwnerId = OwnerId;
            this.Owner = Owner;
            this.CategoryId = CategoryId;
            this.ModelName = ModelName;
            this.CCTS = CCTS;
            this.Floor = Floor;
            this.Wallpaper = Wallpaper;
            this.Landscape = Landscape;
            this.State = RoomAccessUtility.ToRoomAccess(State);
            this.Password = Password;
            this.ShowName = ShowName;
            this.SuperUsers = SuperUsers;
            this.UsersNow = UsersNow;
            this.UsersMax = UsersMax;
        }

        public RoomData(RoomData data)
        {
            this.Id = data.Id;
            this.Name = data.Name;
            this.Description = data.Description;
            this.OwnerId = data.OwnerId;
            this.Owner = data.Owner;
            this.CategoryId = data.CategoryId;
            this.ModelName = data.ModelName;
            this.CCTS = data.CCTS;
            this.Floor = data.Floor;
            this.Wallpaper = data.Wallpaper;
            this.Landscape = data.Landscape;
            this.State = data.State;
            this.Password = data.Password;
            this.ShowName = data.ShowName;
            this.SuperUsers = data.SuperUsers;
            this.UsersNow = data.UsersNow;
            this.UsersMax = data.UsersMax;
        }       

        public RoomModel Model
        {
            get
            {
                if (mModel == null)
                    mModel = PrjTwentySixEnv.GetGame().GetRoomManager().GetModel(ModelName);
                return mModel;
            }
        }
    }
}
