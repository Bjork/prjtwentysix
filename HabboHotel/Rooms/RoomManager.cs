﻿using log4net;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.HabboHotel.Rooms;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel
{
    public class RoomManager
    {
        private static readonly ILog log = LogManager.GetLogger("PrjTwentySix.HabboHotel.Rooms.RoomManager");

        private Dictionary<string, RoomModel> _roomModels;
        private ConcurrentDictionary<int, Room> _rooms;
        private readonly object _roomLoadingSync;

        public RoomManager()
        {
            this._roomModels = new Dictionary<string, RoomModel>();
            this._rooms = new ConcurrentDictionary<int, Room>();
            this._roomLoadingSync = new object();
            this.LoadModels();

            log.Info("Room Manager -> LOADED");
        }

        public void LoadModels()
        {
            if (this._roomModels.Count > 0)
                _roomModels.Clear();

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `room_models`");
                DataTable Data = dbClient.getTable();

                if (Data == null)
                    return;

                foreach (DataRow Row in Data.Rows)
                {
                    _roomModels.Add(Convert.ToString(Row["model"]), new RoomModel(Convert.ToString(Row["model"]), Convert.ToInt32(Row["roomomatic_subscr_only"]), Convert.ToInt32(Row["door_x"]), Convert.ToInt32(Row["door_y"]), 
                        Convert.ToInt32(Row["door_h"]), Convert.ToByte(Row["door_z"]), Convert.ToString(Row["heightmap"]), Convert.ToString(Row["publicroom_items"]), PrjTwentySixEnv.EnumToBool(Row["swimmingpool"].ToString()),
                        Convert.ToString(Row["specialcast_emitter"]), Convert.ToInt32(Row["specialcast_interval"]), Convert.ToInt32(Row["specialcast_rnd_min"]), Convert.ToInt32(Row["specialcast_rnd_max"])));
                }

                log.Info("Loaded " + this._roomModels.Count + " room models.");
            }
        }

        public RoomModel GetModel(string Model)
        {
            return TryGetModel(Model, out RoomModel RoomModel) ? RoomModel : null;
        }

        public bool TryGetModel(string Id, out RoomModel Model)
        {
            if (this._roomModels.ContainsKey(Id))
            {
                Model = this._roomModels[Id];
                return true;
            }

            Model = null;
            return false;
        }

        public bool TryGetRoom(int RoomId, out Room Room)
        {
            return this._rooms.TryGetValue(RoomId, out Room);
        }
    }
}
