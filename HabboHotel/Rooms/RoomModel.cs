﻿using PrjTwentySix.Communication.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Rooms
{
    public class RoomModel
    {
        public string Model;
        public int SubOnly;
        public int DoorH;
        public int DoorX;
        public int DoorY;
        public double DoorZ;

        public string Heightmap;
        public string PublicRoomItems;
        public bool HasSwimmingPool;
        public bool PublicRoom;

        public string specialcast_emitter;
        public int specialCast_interval;
        public int specialcast_rnd_min;
        public int specialcast_rnd_max;

        public int MapSizeX;
        public int MapSizeY;
        public short[,] SqFloorHeight;
        public byte[,] SqSeatRot;
        public SquareState[,] SqState;

        //PublicRooms
        internal byte[,] SqItemRot;
        internal double[,] SqItemHeight;
        internal bool[,] SqUnit;
        internal SquareTrigger[,] SqTrigger;

        public RoomModel(string Model, int SubOnly, int DoorX, int DoorY, int DoorH, byte DoorZ, string Heightmap, string PublicRoomItems, bool HasSwimmingPool, string specialcast_emitter, int specialCast_interval, int specialcast_rnd_min, int specialcast_rnd_max)
        {
            try
            {
                this.DoorX = DoorX;
                this.DoorY = DoorY;
                this.DoorZ = DoorZ;
                this.DoorH = DoorH;

                this.Heightmap = Heightmap.ToLower();

                string[] tmpHeightmap = Heightmap.Split(Convert.ToChar(13));

                this.MapSizeX = tmpHeightmap[0].Length;
                this.MapSizeY = tmpHeightmap.Length;
                this.SubOnly = SubOnly;

                this.PublicRoom = (PublicRoomItems != "");

                SqState = new SquareState[MapSizeX, MapSizeY];
                SqFloorHeight = new short[MapSizeX, MapSizeY];
                SqSeatRot = new byte[MapSizeX, MapSizeY];

                for (int y = 0; y < MapSizeY; y++)
                {
                    string line = tmpHeightmap[y];
                    line = line.Replace("\r", "");
                    line = line.Replace("\n", "");

                    int x = 0;
                    foreach (char square in line)
                    {
                        if (square == 'x')
                        {
                            SqState[x, y] = SquareState.BLOCKED;
                        }
                        else
                        {
                            SqState[x, y] = SquareState.OPEN;
                            SqFloorHeight[x, y] = parse(square);
                        }
                        x++;
                    }
                }

                if (PublicRoom)
                {
                    string[] Items = this.PublicRoomItems.Split('\n');
                    for (int i = 0; i < Items.Length; i++)
                    {
                        string[] itemData = Items[i].Split(' ');
                        int X = int.Parse(itemData[2]);
                        int Y = int.Parse(itemData[3]);
                        SquareState Type;
                        Type = (SquareState)int.Parse(itemData[6]);
                        SqState[X, Y] = Type;

                        if (Type == SquareState.SEAT)
                        {
                            SqItemRot[X, Y] = byte.Parse(itemData[5]);
                            SqItemHeight[X, Y] = 1.0;
                        }

                        PublicRoomItems += itemData[0] + " " + itemData[1] + " " + itemData[2] + " " + itemData[3] + " " + itemData[4] + " " + itemData[5] + Convert.ToChar(13);

                    }

                    using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("SELECT object,x,y,goalx,goaly,stepx,stepy,roomid,state FROM room_models_triggers WHERE model = '" + Model + "'");
                        DataTable Data = dbClient.getTable();

                        if (Data == null)
                            return;

                        foreach (DataRow dbRow in Data.Rows)
                        {
                            SqTrigger[Convert.ToInt32(dbRow["x"]), Convert.ToInt32(dbRow["y"])] = new SquareTrigger(Convert.ToString(dbRow["object"]), Convert.ToInt32(dbRow["goalx"]), Convert.ToInt32(dbRow["goaly"]), Convert.ToInt32(dbRow["stepx"]), Convert.ToInt32(dbRow["stepy"]), (Convert.ToInt32(dbRow["state"]) == 1), Convert.ToInt32(dbRow["roomid"]));
                        }
                    }

                    SqState[DoorX, DoorY] = 0;
                }

            }
            catch { }
        }

        public static short parse(char input)
        {

            switch (input)
            {
                case '0':
                    return 0;
                case '1':
                    return 1;
                case '2':
                    return 2;
                case '3':
                    return 3;
                case '4':
                    return 4;
                case '5':
                    return 5;
                case '6':
                    return 6;
                case '7':
                    return 7;
                case '8':
                    return 8;
                case '9':
                    return 9;
                case 'a':
                    return 10;
                case 'b':
                    return 11;
                case 'c':
                    return 12;
                case 'd':
                    return 13;
                case 'e':
                    return 14;
                case 'f':
                    return 15;
                case 'g':
                    return 16;
                case 'h':
                    return 17;
                case 'i':
                    return 18;
                case 'j':
                    return 19;
                case 'k':
                    return 20;
                case 'l':
                    return 21;
                case 'm':
                    return 22;
                case 'n':
                    return 23;
                case 'o':
                    return 24;
                case 'p':
                    return 25;
                case 'q':
                    return 26;
                case 'r':
                    return 27;
                case 's':
                    return 28;
                case 't':
                    return 29;
                case 'u':
                    return 30;
                case 'v':
                    return 31;
                case 'w':
                    return 32;

                default:
                    throw new FormatException("The input was not in a correct format: input must be between (0-k)");
            }
        }

        public SquareState[,] getSquareState()
        {
            return (SquareState[,])this.SqState.Clone();
        }

        public byte[,] getSqFloorHeight()
        {
            return (byte[,])this.SqFloorHeight.Clone();
        }

        public byte[,] getSqItemRot()
        {
            return (byte[,])SqItemRot.Clone();
        }

        public double[,] getSqItemHeight()
        {
            return (double[,])SqItemHeight.Clone();
        }

        public bool[,] getSqUnit()
        {
            return (bool[,])SqUnit.Clone();
        }

        public SquareTrigger[,] getSqTrigger()
        {
            return (SquareTrigger[,])SqTrigger.Clone();
        }
    }

    public enum SquareState
    {
        OPEN = 0,
        BLOCKED = 1,
        SEAT = 2,
        POOL = 3,
        VIP = 4
    }

    public class SquareTrigger
    {
        internal readonly string Object;
        internal readonly int goalX;
        internal readonly int goalY;
        internal readonly int stepX;
        internal readonly int stepY;
        internal readonly int roomID;
        internal bool State;

        internal SquareTrigger(string Object, int goalX, int goalY, int stepX, int stepY, bool State, int roomID)
        {
            this.Object = Object;
            this.goalX = goalX;
            this.goalY = goalY;
            this.stepX = stepX;
            this.stepY = stepY;
            this.roomID = roomID;
            this.State = State;
        }
    }
}
