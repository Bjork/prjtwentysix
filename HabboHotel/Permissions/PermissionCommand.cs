﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Permissions
{
    class PermissionCommand
    {
        public string Command { get; set; }
        public int MinRank { get; set; }

        public PermissionCommand(string Command, int MinRank)
        {
            this.Command = Command;
            this.MinRank = MinRank;
        }
    }
}
