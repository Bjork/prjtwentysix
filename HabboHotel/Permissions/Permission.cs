﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Permissions
{
    class Permission
    {
        public int MinRank { get; set; }
        public string Perm { get; set; }

        public Permission(int MinRank, string Permission)
        {
            this.MinRank = MinRank;
            this.Perm = Permission;
        }
    }
}
