﻿using log4net;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Permissions
{
    public sealed class PermissionManager
    {
        private static readonly ILog log = LogManager.GetLogger("PrjTwentySix.HabboHotel.Permissions.PermissionManager");

        private readonly Dictionary<int, Permission> _permissions = new Dictionary<int, Permission>();
        private readonly Dictionary<string, PermissionCommand> _commands = new Dictionary<string, PermissionCommand>();

        public PermissionManager() { }

        public void Init()
        {
            this._permissions.Clear();
            this._commands.Clear();

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions_system`");
                DataTable GetPermissions = dbClient.getTable();

                if (GetPermissions != null)
                {
                    foreach (DataRow Row in GetPermissions.Rows)
                    {
                        this._permissions.Add(Convert.ToInt32(Row["id"]), new Permission(Convert.ToInt32(Row["min_rank"]), Convert.ToString(Row["permission"])));
                    }
                }
            }

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `permissions_commands`");
                DataTable GetCommands = dbClient.getTable();

                if (GetCommands != null)
                {
                    foreach (DataRow Row in GetCommands.Rows)
                    {
                        this._commands.Add(Convert.ToString(Row["command"]), new PermissionCommand(Convert.ToString(Row["command"]), Convert.ToInt32(Row["min_rank"])));
                    }
                }
            }        

            log.Info("Loaded " + this._permissions.Count + " permissions.");
            log.Info("Loaded " + this._commands.Count + " commands.");
        }

        public List<string> GetPermissionsForPlayer(Habbo Player)
        {
            List<string> _permissionSet = new List<string>();
            List<string> _userFuserights = new List<string>();

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT permission FROM user_permissions WHERE user_id = " + Player.Id);
                DataTable GetPermissions = dbClient.getTable();

                foreach (DataRow dbRow in GetPermissions.Rows)
                    _userFuserights.Add(Convert.ToString(dbRow[0]));
            }

            foreach (KeyValuePair<int, Permission> permission in _permissions.Where(x => x.Value.MinRank <= Player.Rank).ToList())
            {
                _permissionSet.Add(permission.Value.Perm + Convert.ToChar(2));
            }

            foreach (string permission in _userFuserights)
            {
                _permissionSet.Add(permission + Convert.ToChar(2));
            }

            return _permissionSet;
        }

        public List<string> GetCommandsForPlayer(Habbo Player)
        {
            return this._commands.Where(x => Player.Rank >= x.Value.MinRank).Select(x => x.Key).ToList();
        }
    }
}
