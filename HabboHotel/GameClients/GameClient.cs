﻿using PrjTwentySix.Communication.Connection;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.Communication.Packets.Incoming;
using PrjTwentySix.Communication.Packets.Outgoing;
using PrjTwentySix.Communication.Packets.Outgoing.Handshake;
using PrjTwentySix.Core.Logging;
using PrjTwentySix.HabboHotel.Users;
using PrjTwentySix.HabboHotel.Users.UserDatas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.GameClients
{
    public class GameClient
    {
        private readonly int _id;
        public string MachineId;
        private bool _disconnected;
        private Habbo _habbo;
        private GamePacketParser _packetParser;
        private ConnectionInformation _connection;
        public int PingCount { get; set; }
        public bool PingOk = false;

        public GameClient(int ClientId, ConnectionInformation pConnection)
        {
            this._id = ClientId;
            this._connection = pConnection;
            this._packetParser = new GamePacketParser(this);

            this.PingCount = 0;            
        }

        private void SwitchParserRequest()
        {
            _packetParser.SetConnection(_connection);
            _packetParser.onNewPacket += parser_onNewPacket;
            byte[] data = (_connection.parser as InitialPacketParser).currentData;
            _connection.parser.Dispose();
            _connection.parser = _packetParser;
            _connection.parser.handlePacketData(data);
        }

        private void parser_onNewPacket(ClientPacket Message)
        {
            try
            {
                PrjTwentySixEnv.GetGame().GetPacketManager().TryExecutePacket(this, Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("errrror: " + e);
                //Logging.LogPacketException(Message.ToString(), e.ToString());
            }
        }

        public void StartConnection()
        {
            if (_connection == null)
                return;

            this.PingCount = 0;
            (_connection.parser as InitialPacketParser).SwitchParserRequest += SwitchParserRequest;
            _connection.startPacketProcessing();
            sendData("@@");
        }

        public bool TryAuthenticate(string AuthTicket)
        {
            try
            {
                byte errorCode = 0;
                UserData userData = UserDataFactory.GetUserData(AuthTicket, out errorCode);
                if (errorCode == 1 || errorCode == 2)
                {
                    Console.WriteLine("Trouvé personne... / ErrorCode: " + errorCode);
                    Disconnect();
                    return false;
                }

                if (userData.user == null) //Possible NPE
                    return false;

                //Ban Checking

                PrjTwentySixEnv.GetGame().GetClientManager().RegisterClient(this, userData.userID, userData.user.Username);
                _habbo = userData.user;

                if (_habbo != null)
                {
                    userData.user.Init(this, userData);
                    SendMessage(new UserRightsComposer(this));
                    SendMessage(new AuthenticationOkComposer());
                    SendMessage(new HotelAlertComposer("Bienvenue sur prjTwentySix #USERNAME#!".Replace("#USERNAME#", _habbo.Username)));
                }
                return true;
            }
            catch (InvalidCastException e)
            {
                Logging.LogCriticalException("Bug during user login: " + e + " / test: " + e.Message);
            }
            return false;
        }

        public void SendMessage(IServerPacket Message)
        {
            string packet = Message.GetBody();

            if (Message == null)
                return;

            if (GetConnection() == null)
                return;

            //Console.WriteLine("Packet: " + packet);

            byte[] dataBytes = Encoding.Default.GetBytes(packet + Convert.ToChar(1));
            GetConnection().SendData(dataBytes);
        }

        internal void sendData(string Data)
        {
            try
            {
                //Console.WriteLine("Packet: " + Data);

                byte[] dataBytes = Encoding.Default.GetBytes(Data + Convert.ToChar(1));
                GetConnection().SendData(dataBytes);
            }
            catch
            {
                Disconnect();
            }
        }

        public int ConnectionID
        {
            get { return _id; }
        }

        public ConnectionInformation GetConnection()
        {
            return _connection;
        }

        public Habbo GetHabbo()
        {
            return _habbo;
        }

        public void Disconnect()
        {
            try
            {
                if (this != null && GetHabbo() != null)
                {
                    GetHabbo().OnDisconnect();
                }
            }
            catch (Exception e)
            {
                Logging.LogException(e.ToString());
            }


            if (!_disconnected)
            {
                if (_connection != null)
                    _connection.Dispose();
                _disconnected = true;
            }
        }


        public void Dispose()
        {
            if (GetHabbo() != null)
                GetHabbo().OnDisconnect();

            this.MachineId = string.Empty;
            this._disconnected = true;
            this._habbo = null;
            this._connection = null;
            this._packetParser = null;
        }
    }
}
