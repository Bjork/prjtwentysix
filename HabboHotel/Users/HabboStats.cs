﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Users
{
    class HabboStats
    {
        public int RoomVisits { get; set; }
        public double OnlineTime { get; set; }
        public int Respect { get; set; }
        public int RespectGiven { get; set; }
        public int GiftsGiven { get; set; }
        public int GiftsReceived { get; set; }

        public HabboStats(int roomVisits, double onlineTime, int Respect, int respectGiven, int giftsGiven, int giftsReceived)
        {
            this.RoomVisits = roomVisits;
            this.OnlineTime = onlineTime;
            this.Respect = Respect;
            this.RespectGiven = respectGiven;
            this.GiftsGiven = giftsGiven;
            this.GiftsReceived = giftsReceived;
        }
    }
}
