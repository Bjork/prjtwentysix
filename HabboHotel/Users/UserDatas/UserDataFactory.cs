﻿using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.HabboHotel.Users.Authenticator;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Users.UserDatas
{
    public class UserDataFactory
    {
        public static UserData GetUserData(string SessionTicket, out byte errorCode)
        {
            int UserId;
            DataRow dUserInfo = null;
            DataRow UserInfo = null;

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `users` WHERE `sso_ticket` = @sso LIMIT 1");
                dbClient.AddParameter("sso", SessionTicket);
                dUserInfo = dbClient.getRow();
                if (dUserInfo == null)
                {
                    errorCode = 1;
                    return null;
                }

                UserId = Convert.ToInt32(dUserInfo["id"]);
                if (PrjTwentySixEnv.GetGame().GetClientManager().GetClientByUserID(UserId) != null)
                {
                    errorCode = 2;
                    PrjTwentySixEnv.GetGame().GetClientManager().GetClientByUserID(UserId).Disconnect();
                    return null;
                }

                dbClient.SetQuery("SELECT * FROM `user_info` WHERE `user_id` = '" + UserId + "' LIMIT 1");
                UserInfo = dbClient.getRow();
                if (UserInfo == null)
                {
                    dbClient.RunQuery("INSERT INTO `user_info` (`user_id`, `reg_timestamp`) VALUES ('" + UserId + "', '" + PrjTwentySixEnv.GetUnixTimestamp() + "')");

                    dbClient.SetQuery("SELECT * FROM `user_info` WHERE `user_id` = '" + UserId + "' LIMIT 1");
                    UserInfo = dbClient.getRow();
                }

                dbClient.RunQuery("UPDATE `users` SET `online` = '1', `sso_ticket` = '123', last_online_timestamp = '" + PrjTwentySixEnv.GetUnixTimestamp() + "' WHERE `id` = '" + UserId + "' LIMIT 1");
                dbClient.RunQuery("UPDATE `user_info` SET `login_timestamp` = '" + PrjTwentySixEnv.GetUnixTimestamp() + "' WHERE user_id = '" + UserId + "' LIMIT 1");
            }           

            Habbo user = HabboFactory.GenerateHabbo(dUserInfo, UserInfo);

            dUserInfo = null;           

            errorCode = 0;
            return new UserData(UserId, user);
        } 
    }
}
