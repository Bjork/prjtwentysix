﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Users.UserDatas
{
    public class UserData
    {
        public int userID;
        public Habbo user;

        public UserData(int userID, Habbo user)
        {
            this.userID = userID;
            this.user = user;            
        }
    }
}
