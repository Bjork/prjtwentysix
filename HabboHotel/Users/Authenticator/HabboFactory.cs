﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Users.Authenticator
{
    public static class HabboFactory
    {
        internal static Habbo GenerateHabbo(DataRow Row, DataRow UserInfo)
        {
            return new Habbo(Convert.ToInt32(Row["id"]),
                Convert.ToString(Row["username"]),
                Convert.ToInt32(Row["rank"]),
                Convert.ToString(Row["motto"]),
                Convert.ToString(Row["consolemotto"]),
                Convert.ToString(Row["look"]),
                Convert.ToString(Row["swim_look"]),
                Convert.ToString(Row["gender"]),
                Convert.ToString(Row["ip_last"]),
                Convert.ToInt32(Row["credits"]),
                Convert.ToInt32(Row["tickets"]),
                Convert.ToInt32(Row["photos"])
                );
        }
    }
}