﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.Core.Logging;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Users.Permissions;
using PrjTwentySix.HabboHotel.Users.UserDatas;

namespace PrjTwentySix.HabboHotel.Users
{
    public class Habbo
    {
        private int _Id;
        private string _Username;
        private int _Rank;
        private string _Motto;
        private string _ConsoleMotto;
        private string _Look;
        private string _SwimLook;
        private string _Gender;
        private string _IP;
        private int _Credits;
        private int _Tickets;
        private int _Photos;

        private bool _ClubMember;

        private bool _disconnected;

        private GameClient _client;
        private HabboStats _habboStats;
        private PermissionComponent _permissions;

        public Habbo(int Id, string Username, int Rank, string Motto, string ConsoleMotto, string Look, string SwimLook, string Gender, string IP, int Credits, int Tickets, int Photos)
        {
            this._Id = Id;
            this._Username = Username;
            this._Rank = Rank;
            this._Motto = Motto;
            this._ConsoleMotto = ConsoleMotto;
            this._Look = Look;
            this._SwimLook = SwimLook;
            this._Gender = Gender;
            this._IP = IP;
            this._Credits = Credits;
            this._Tickets = Tickets;
            this._Photos = Photos;

            this._ClubMember = false;

            this.InitPermissions();

            #region Stats
            DataRow StatRow = null;
            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `user_stats` WHERE `id` = @user_id LIMIT 1");
                dbClient.AddParameter("user_id", Id);
                StatRow = dbClient.getRow();

                if (StatRow == null)//No row, add it yo
                {
                    dbClient.RunQuery("INSERT INTO `user_stats` (`id`) VALUES ('" + Id + "')");
                    dbClient.SetQuery("SELECT * FROM `user_stats` WHERE `id` = @user_id LIMIT 1");
                    dbClient.AddParameter("user_id", Id);
                    StatRow = dbClient.getRow();
                }

                try
                {
                    this._habboStats = new HabboStats(Convert.ToInt32(StatRow["RoomVisits"]), 
                        Convert.ToDouble(StatRow["OnlineTime"]), Convert.ToInt32(StatRow["Respect"]), 
                        Convert.ToInt32(StatRow["RespectGiven"]), Convert.ToInt32(StatRow["GiftsGiven"]),
                        Convert.ToInt32(StatRow["GiftsReceived"]));
                }
                catch (Exception e)
                {
                    Logging.LogException(e.ToString());
                }
                #endregion

                this._disconnected = false;
            }
        }

        public int Id
        {
            get { return this._Id; }
            set { this._Id = value; }
        }

        public string Username
        {
            get { return this._Username; }
            set { this._Username = value; }
        }

        public int Rank
        {
            get { return this._Rank; }
            set { this._Rank = value; }
        }

        public string Motto
        {
            get { return this._Motto; }
            set { this._Motto = value; }
        }

        public string ConsoleMotto
        {
            get { return this._ConsoleMotto; }
            set { this._ConsoleMotto = value; }
        }

        public string Look
        {
            get { return this._Look; }
            set { this._Look = value; }
        }

        public string SwimLook
        {
            get { return this._SwimLook; }
            set { this._SwimLook = value; }
        }

        public string Gender
        {
            get { return this._Gender; }
            set { this._Gender = value; }
        }

        public string IP
        {
            get { return this._IP; }
            set { this._IP = value; }
        }

        public int Credits
        {
            get { return this._Credits; }
            set { this._Credits = value; }
        }

        public int Tickets
        {
            get { return this._Tickets; }
            set { this._Tickets = value; }
        }

        public int Photos
        {
            get { return this._Photos; }
            set { this._Photos = value; }
        }

        public bool ClubMember
        {
            get { return this._ClubMember; }
            set { this._ClubMember = value; }
        }

        public void Init(GameClient client, UserData data)
        {
            /*this.FavoriteRooms = new ArrayList();
            foreach (int id in data.favouritedRooms)
            {
                FavoriteRooms.Add(id);
            }

            this._client = client;
            BadgeComponent = new BadgeComponent(this, data);
            InventoryComponent = new InventoryComponent(Id, client);

            Messenger = new HabboMessenger(Id);
            Messenger.Init(data.friends, data.requests);
            this._friendCount = Convert.ToInt32(data.friends.Count);*/



            this._disconnected = false;
        }

        private bool InitPermissions()
        {
            this._permissions = new PermissionComponent();
            if (this._permissions.Init(this))
                return true;
            return false;
        }

        public PermissionComponent GetPermissions()
        {
            return this._permissions;
        }

        public void OnDisconnect()
        {
            if (this._disconnected)
                return;          

            this._disconnected = true;

            PrjTwentySixEnv.GetGame().GetClientManager().UnregisterClient(Id, Username);

            /*if (!this._habboSaved)
            {
                this._habboSaved = true;
                using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE `users` SET `online` = '0', `last_online_timestamp` = '" + PlusEnvironment.GetUnixTimestamp() + "', `activity_points` = '" + this.Duckets + "', `credits` = '" + this.Credits + "', `vip_points` = '" + this.Diamonds + "',  `bonus_points` = '" + this.BonusPoints + "' , `home_room` = '" + this.HomeRoom + "', `time_muted` = '" + this.TimeMuted + "',`friend_bar_state` = '" + FriendBarStateUtility.GetInt(this._friendbarState) + "', daily_respect_points = '" + this.DailyRespectPoints + "', daily_pet_respect_points = '" + this.DailyPetRespectPoints + "' WHERE id = '" + Id + "' LIMIT 1; UPDATE `user_stats` SET `RoomVisits` = '" + this._habboStats.RoomVisits + "', `OnlineTime` = '" + (PlusEnvironment.GetUnixTimestamp() - SessionStart + this._habboStats.OnlineTime) + "', `Respect` = '" + this._habboStats.Respect + "', `RespectGiven` = '" + this._habboStats.RespectGiven + "', `GiftsGiven` = '" + this._habboStats.GiftsGiven + "', `GiftsReceived` = '" + this._habboStats.GiftsReceived + "', `AchievementScore` = '" + this._habboStats.AchievementPoints + "', `groupid` = '" + this._habboStats.FavouriteGroupId + "', ForumMessages = '" + this._habboStats.ForumPostsCount + "' WHERE `id` = '" + this.Id + "' LIMIT 1;");

                    if (GetPermissions().HasRight("mod_tickets"))
                        dbClient.RunQuery("UPDATE `moderation_tickets` SET `status` = 'open', `moderator_id` = '0' WHERE `status` ='picked' AND `moderator_id` = '" + Id + "'");
                }                
            }*/

            Logging.WriteLine(Username + " déconnecté [IP: " + IP + "]", ConsoleColor.DarkRed);
            
            this.Dispose();
            this._client = null;
        }

        public void Dispose()
        {
            if (this._permissions != null)
                this._permissions.Dispose();
        }
    }
}
