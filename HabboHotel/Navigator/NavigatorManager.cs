﻿using log4net;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Navigator
{
    public sealed class NavigatorManager
    {
        private static readonly ILog log = LogManager.GetLogger("PrjTwentySix.HabboHotel.Navigator.NavigatorManager");

        private Dictionary<int, RoomCategory> _roomCategories = new Dictionary<int, RoomCategory>();
        private Dictionary<int, RecommendedRoom> _recommendedRooms = new Dictionary<int, RecommendedRoom>();

        public NavigatorManager()
        {
            this._roomCategories = new Dictionary<int, RoomCategory>();
            this._recommendedRooms = new Dictionary<int, RecommendedRoom>();
        }

        public void Init()
        {
            if (this._roomCategories.Count > 0)
                this._roomCategories.Clear();

            if (this._recommendedRooms.Count > 0)
                this._recommendedRooms.Clear();

            DataTable Categories = null;
            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM room_categories ORDER BY id ASC");
                Categories = dbClient.getTable();

                if (Categories != null)
                {
                    foreach (DataRow Row in Categories.Rows)
                    {
                        if (Convert.ToInt32(Row["enabled"]) == 1)
                        {
                            if (!this._roomCategories.ContainsKey(Convert.ToInt32(Row["id"])))
                                this._roomCategories.Add(Convert.ToInt32(Row["id"]), new RoomCategory(Convert.ToInt32(Row["id"]), Convert.ToInt32(Row["order_id"]), Convert.ToInt32(Row["parent_id"]), Convert.ToInt32(Row["isnode"]), Convert.ToString(Row["name"]), Convert.ToInt32(Row["public_spaces"]), Convert.ToInt32(Row["minrole_access"]), Convert.ToInt32(Row["minrole_setflatcat"]), PrjTwentySixEnv.EnumToBool(Row["allow_trading"].ToString())));
                        }
                    }
                }

                dbClient.SetQuery("SELECT * FROM `room_recommended`");
                DataTable GetRecommended = dbClient.getTable();

                if (GetRecommended != null)
                {
                    foreach (DataRow Row in GetRecommended.Rows)
                    {
                        if (Convert.ToInt32(Row["enabled"]) == 1)
                        {
                            if (!this._recommendedRooms.ContainsKey(Convert.ToInt32(Row["id"])))
                                this._recommendedRooms.Add(Convert.ToInt32(Row["id"]), new RecommendedRoom(Convert.ToInt32(Row["id"]), Convert.ToInt32(Row["room_id"])));
                        }
                    }
                }

                log.Info("Loaded " + this._roomCategories.Count + " room categories.");
                log.Info("Loaded " + this._recommendedRooms.Count + " recommended rooms.");
            }
        }

        public RoomCategory GetRoomCategory(int Id)
        {
            if (this._roomCategories.ContainsKey(Id))
            {
                RoomCategory Category = _roomCategories[Id];
                return Category;
            }
            else
                return null;
        }

        public List<RoomCategory> GetChildRoomCategories(int ParentId)
        {
            List<RoomCategory> Categories = new List<RoomCategory>();
            foreach (RoomCategory Category in _roomCategories.Values)
            {
                if (Category.ParentId == ParentId)
                {
                    Categories.Add(Category);
                }
            }

            return Categories;
        }

        public List<RoomCategory> GetAvailableFlatCategories(GameClient Session)
        {
            List<RoomCategory> Categories = new List<RoomCategory>();
            foreach (RoomCategory Category in _roomCategories.Values)
            {
                if (Session.GetHabbo().Rank >= Category.MinRankAccess)
                    Categories.Add(Category);
            }
            return Categories;
        }

        public ICollection<RecommendedRoom> GetRecommendedRooms()
        {
            List<RecommendedRoom> RecommendedRooms = new List<RecommendedRoom>();
            foreach (var RecommendedRoom in this._recommendedRooms)
            {
                if (!RecommendedRooms.Contains(RecommendedRoom.Value))
                    RecommendedRooms.Add(RecommendedRoom.Value);
            }
            return RecommendedRooms;
        }
    }
}
