﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Navigator
{
    public class RoomCategory
    {
        private int _Id;
        private int _OrderId;
        private int _ParentId;
        private string _Caption;
        private bool _isNode;
        private bool _canTrade;
        private int _PublicSpaces;
        private int _MinRankAccess;
        private int _MinRankSetFlat;

        public RoomCategory(int Id, int OrderId, int ParentId, int isNode, string Caption, int PublicSpaces, int MinRankAccess, int MinRankSetFlat, bool canTrade)
        {
            this._Id = Id;
            this._OrderId = OrderId;
            this._ParentId = ParentId;
            this._Caption = Caption;
            this._isNode = isNode == 1;
            this._canTrade = canTrade;
            this._PublicSpaces = PublicSpaces;
            this._MinRankAccess = MinRankAccess;
            this._MinRankSetFlat = MinRankSetFlat;
        }

        public int Id
        {
            get { return this._Id; }
            set { this._Id = value; }
        }

        public int OrderId
        {
            get { return this._OrderId; }
            set { this._OrderId = value; }
        }

        public int ParentId
        {
            get { return this._ParentId; }
            set { this._ParentId = value; }
        }
        
        public string Caption
        {
            get { return this._Caption; }
            set { this._Caption = value; }
        }

        public bool IsNode
        {
            get { return this._isNode; }
            set { this._isNode = value; }
        }

        public bool CanTrade
        {
            get { return this._canTrade; }
            set { this._canTrade = value; }
        }

        public int PublicSpaces
        {
            get { return this._PublicSpaces; }
            set { this._PublicSpaces = value; }
        }


        public int MinRankAccess
        {
            get { return this._MinRankAccess; }
            set { this._MinRankAccess = value; }
        }

        public int MinRankSetFlat
        {
            get { return this._MinRankSetFlat; }
            set { this._MinRankSetFlat = value; }
        }        
    }
}
