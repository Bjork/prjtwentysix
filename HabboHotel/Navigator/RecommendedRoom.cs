﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel.Rooms
{
    public class RecommendedRoom
    {
        public int Id { get; set; }
        public int RoomId { get; set; }

        public RecommendedRoom(int id, int roomId)
        {
            this.Id = id;
            this.RoomId = roomId;
        }
    }
}
