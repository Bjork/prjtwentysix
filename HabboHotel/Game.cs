﻿using log4net;
using PrjTwentySix.Communication.Packets;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Navigator;
using PrjTwentySix.HabboHotel.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.HabboHotel
{
    public class Game
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.HabboHotel.Game");

        private readonly PacketManager _packetManager;
        private readonly GameClientManager _clientManager;
        private readonly PermissionManager _permissionManager;
        private readonly NavigatorManager _navigatorManager;
        private readonly RoomManager _roomManager;

        public Game()
        {
            this._clientManager = new GameClientManager();
            this._packetManager = new PacketManager();

            this._navigatorManager = new NavigatorManager();
            this._navigatorManager.Init();

            this._roomManager = new RoomManager();

            this._permissionManager = new PermissionManager();
            this._permissionManager.Init();
        }

        public PacketManager GetPacketManager()
        {
            return _packetManager;
        }

        public GameClientManager GetClientManager()
        {
            return _clientManager;
        }

        public PermissionManager GetPermissionManager()
        {
            return _permissionManager;
        }

        public NavigatorManager GetNavigatorManager()
        {
            return _navigatorManager;
        }

        public RoomManager GetRoomManager()
        {
            return _roomManager;
        }
    }
}
