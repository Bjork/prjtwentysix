﻿using PrjTwentySix.Communication.Connection.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Connection
{
    public class InitialPacketParser : IDataParser
    {
        public delegate void NoParamDelegate();

        public byte[] currentData;

        public void handlePacketData(byte[] packet)
        {
            if (packet[0] != 67 && SwitchParserRequest != null)
            {
                currentData = packet;
                SwitchParserRequest.Invoke();
            }
        }

        public void Dispose()
        {
            PolicyRequest = null;
            SwitchParserRequest = null;
            GC.SuppressFinalize(this);
        }

        public object Clone()
        {
            return new InitialPacketParser();
        }

        public event NoParamDelegate PolicyRequest;
        public event NoParamDelegate SwitchParserRequest;
    }
}
