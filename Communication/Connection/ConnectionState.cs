﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Connection
{
    public enum ConnectionState
    {
        OPEN = 0,
        CLOSED = 1,
        MALFUNCTIONING_PACKET = 2
    }
}
