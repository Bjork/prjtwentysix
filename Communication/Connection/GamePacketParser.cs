﻿using log4net;
using PrjTwentySix.Communication.Connection.Interfaces;
using PrjTwentySix.Communication.Packets.Incoming;
using PrjTwentySix.Core.Encoding;
using PrjTwentySix.HabboHotel.GameClients;
using System;
using System.IO;

namespace PrjTwentySix.Communication.Connection
{
    public class GamePacketParser : IDataParser
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.Net.GamePacketParser");

        public delegate void HandlePacket(ClientPacket message);

        private readonly GameClient currentClient;
        private ConnectionInformation con;

        public GamePacketParser(GameClient me)
        {
            currentClient = me;
        }

        public void handlePacketData(byte[] Data)
        {
            try
            {
                using (BinaryReader Reader = new BinaryReader(new MemoryStream(Data)))
                {
                    System.Text.StringBuilder DataBuffer = new System.Text.StringBuilder();
                    DataBuffer.Append(System.Text.Encoding.Default.GetString(Data, 0, Data.Length));

                    if (DataBuffer.ToString() == "" || DataBuffer.ToString().Length < 3 || DataBuffer.ToString().Contains("\x01") || DataBuffer.ToString().Contains("\x02") || DataBuffer.ToString().Contains("\x05") || DataBuffer.ToString().Contains("\x09"))
                    {
                        currentClient.Disconnect();
                        return;
                    }

                    int v = HabboEncoding.decodeB64(DataBuffer.ToString().Substring(1, 2));
                    string Content = DataBuffer.ToString().Substring(3, v);
                    int packetID = HabboEncoding.decodeB64(Content.Substring(0, 2));

                    ClientPacket Message = new ClientPacket(packetID, Content.Substring(2));
                    onNewPacket.Invoke(Message);

                    DataBuffer.Remove(0, 3 + v);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("error: " + e);
            }
        }

        public void Dispose()
        {
            onNewPacket = null;
            GC.SuppressFinalize(this);
        }

        public object Clone()
        {
            return new GamePacketParser(currentClient);
        }

        public event HandlePacket onNewPacket;

        public void SetConnection(ConnectionInformation con)
        {
            this.con = con;
            onNewPacket = null;
        }
    }
}
