﻿using PrjTwentySix.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Connection
{
    class ConnectionHandling
    {
        private readonly ConnectionManager manager;

        public ConnectionHandling(int port, int maxConnections, int connectionsPerIP, bool enabeNagles)
        {
            manager = new ConnectionManager();
            manager.init(port, maxConnections, connectionsPerIP, new InitialPacketParser(), !enabeNagles);
        }

        public void init()
        {
            manager.connectionEvent += manager_connectionEvent;
            manager.initializeConnectionRequests();
        }

        private void manager_connectionEvent(ConnectionInformation connection)
        {
            connection.connectionChanged += connectionChanged;

            Console.WriteLine("Connection from " + connection.getIp() + " (ID: " + connection.getConnectionID() + ")");

            PrjTwentySixEnv.GetGame().GetClientManager().CreateAndStartClient(Convert.ToInt32(connection.getConnectionID()), connection);
        }

        private void connectionChanged(ConnectionInformation information, ConnectionState state)
        {
            if (state == ConnectionState.CLOSED)
            {
                CloseConnection(information);
            }
        }

        private void CloseConnection(ConnectionInformation Connection)
        {
            try
            {
                Connection.Dispose();
                PrjTwentySixEnv.GetGame().GetClientManager().DisposeConnection(Convert.ToInt32(Connection.getConnectionID()));
            }
            catch (Exception e)
            {
                Logging.LogException(e.ToString());
            }
        }

        public void Destroy()
        {
            manager.destroy();
        }
    }
}
