﻿namespace Plus.Communication.Packets.Incoming
{
    public static class ClientPacketHeader
    {
        //HandShake
        public const int pingReceivedMessageEvent = 196;
        public const int initCryptoEvent = 206;
        public const int packetSSOMessageEvent = 2002;
        public const int initializePlayerMessageEvent = 204;
        public const int pingMessageEvent = 315;

        //Users
        public const int getClubStatusMessageEvent = 26;
        public const int getUserInfoMessageEvent = 7;

        //Navigator
        public const int refreshNavigatorMessageEvent = 150;
        public const int populateNavigatorMessageEvent = 151;
        public const int getRecommendedRoomsMessageEvent = 264;
        public const int getEventsDataMessageEvent = 321;
    }
}