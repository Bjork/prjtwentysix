﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.Communication.Packets.Outgoing.Users;
using PrjTwentySix.HabboHotel.GameClients;

namespace PrjTwentySix.Communication.Packets.Incoming.Users
{
    class GetClubStatusEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;
            
            int restingDays = 0;
            int passedMonths = 0;
            int restingMonths = 0;

            using (IQueryAdapter dbClient = PrjTwentySixEnv.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `user_club` WHERE user_id = " + Session.GetHabbo().Id + " LIMIT 1");
                DataRow _userclub = dbClient.getRow();

                if (_userclub != null)
                {
                    passedMonths = Convert.ToInt32(_userclub["months_expired"]);
                    restingMonths = Convert.ToInt32(_userclub["months_left"]) - 1;
                    restingDays = (int)(DateTime.Parse(Convert.ToString(_userclub["date_monthstarted"]), new System.Globalization.CultureInfo("en-GB"))).Subtract(DateTime.Now).TotalDays + 32;
                    Session.GetHabbo().ClubMember = true;
                }
            }

            Session.SendMessage(new ClubStatusComposer(restingDays, passedMonths, restingMonths));
        }
    }
}
