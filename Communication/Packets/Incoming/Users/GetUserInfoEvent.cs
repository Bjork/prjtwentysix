﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrjTwentySix.Communication.Database.Interfaces;
using PrjTwentySix.Communication.Packets.Outgoing.Handshake;
using PrjTwentySix.Communication.Packets.Outgoing.Inventory.Purse;
using PrjTwentySix.Communication.Packets.Outgoing.Users;
using PrjTwentySix.HabboHotel.GameClients;

namespace PrjTwentySix.Communication.Packets.Incoming.Users
{
    class GetUserInfoEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            Session.SendMessage(new UserInfoComposer(Session));
            Session.SendMessage(new CreditBalanceComposer(Session.GetHabbo().Credits));
            Session.SendMessage(new TicketBalanceComposer(Session.GetHabbo().Tickets));
        }
    }
}
