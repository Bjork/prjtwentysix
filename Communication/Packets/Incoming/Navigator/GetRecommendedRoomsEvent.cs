﻿using PrjTwentySix.Communication.Packets.Outgoing.Navigator;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Incoming.Navigator
{
    class GetRecommendedRoomsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            ICollection<RecommendedRoom> RecommendedRooms = PrjTwentySixEnv.GetGame().GetNavigatorManager().GetRecommendedRooms();

           Session.SendMessage(new GetRecommendedRoomsComposer(RecommendedRooms));
        }
    }
}
