﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrjTwentySix.Communication.Packets.Outgoing.Navigator;
using PrjTwentySix.Core.Encoding;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Navigator;

namespace PrjTwentySix.Communication.Packets.Incoming.Navigator
{
    class RefreshNavigatorEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            int HideFullRooms = HabboEncoding.decodeVL64(Packet.getBody().Substring(0, 1));
            int CategoryId = Packet.getWiredParameters()[1];

            RoomCategory Category = PrjTwentySixEnv.GetGame().GetNavigatorManager().GetRoomCategory(CategoryId);

            if (Category == null || Session.GetHabbo().Rank < Category.MinRankAccess)
            {
                Console.WriteLine("blblblbl");
                return;
            }

            Session.SendMessage(new RefreshNavigatorComposer(Session, Category, HideFullRooms));
        }
    }
}
