﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrjTwentySix.Communication.Packets.Outgoing.Navigator;
using PrjTwentySix.Core.Encoding;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Navigator;

namespace PrjTwentySix.Communication.Packets.Incoming.Navigator
{
    class PopulateNavigatorEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            List<RoomCategory> Categories = PrjTwentySixEnv.GetGame().GetNavigatorManager().GetAvailableFlatCategories(Session);

            Session.SendMessage(new PopulateNavigatorComposer(Categories));
        }
    }
}
