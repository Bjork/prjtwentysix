﻿using PrjTwentySix.Core.Encoding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Incoming
{
    public class ClientPacket
    {
        private string Body;
        private int MessageId;

        public ClientPacket(int messageID, string body)
        {
            Init(messageID, body);
        }

        public int Id
        {
            get { return MessageId; }
        }

        public int Header
        {
            get { return MessageId; }
        }

        public string getBody()
        {   
            return Body;
        }

        public void Init(int messageID, string body)
        {
            if (body == null)
                body = "";

            MessageId = messageID;
            Body = body;
        }

        public override string ToString()
        {
            return "[" + Header + "] BODY: " + Body;
        }

        public string getStructuredParameter(int paramID)
        {
            return HabboEncoding.getStructuredParameter(Body, paramID);
        }

        public string getParameter(int parameterLocation)
        {
            return HabboEncoding.getParameter(Body, parameterLocation);
        }

        /// <summary>
        /// Wire decodes the message content and returns it as an integer.
        /// </summary>
        /// <returns></returns>
        public int getNextWiredParameter()
        {
            try
            {
                return HabboEncoding.WireDecode(ref Body);
            }
            catch { return 0; }
        }

        /// <summary>
        /// Decodes the whole message content (assumed that it's a wired string of encoded numbers) to an integer array. An empty array is returned on errors.
        /// </summary>
        public int[] getWiredParameters()
        {
            return HabboEncoding.decodeWire(Body);
        }

        public string[] getMixedParameters()
        {
            return HabboEncoding.getMixedParameters(Body);
        }

        public int[] getNumericMixedParameters()
        {
            return HabboEncoding.getMixedParametersAsIntegers(Body);
        }
    }
}
