﻿using PrjTwentySix.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Incoming.Handshake
{
    class GenerateKeyEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new GenerateKeyComposer());
        }
    }
}