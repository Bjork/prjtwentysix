﻿using PrjTwentySix.Communication.Packets.Incoming;
using PrjTwentySix.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using System;

namespace PrjTwentySix.Communication.Packets
{
    internal class InitCryptoEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new InitCryptoComposer());
        }
    }
}