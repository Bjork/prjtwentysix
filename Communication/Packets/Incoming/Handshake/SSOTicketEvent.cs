﻿using PrjTwentySix.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Incoming.Handshake
{
    class SSOTicketEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string SSOTicket = Packet.getBody().Substring(2);

            if (Session == null || Session.GetHabbo() != null)
                return;

            if (string.IsNullOrEmpty(SSOTicket) || string.IsNullOrWhiteSpace(SSOTicket) /*|| SSOTicket.Length <= 20*/)
                return;

            Session.TryAuthenticate(SSOTicket);
        }
    }
}