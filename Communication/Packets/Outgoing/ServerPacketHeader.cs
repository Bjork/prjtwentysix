﻿// RELEASED BY MARKO97 - PRODUCTION-201802201205-141713395

namespace Plus.Communication.Packets.Outgoing
{
    public static class ServerPacketHeader
    {
        public const int InitCryptoMessageComposer = 277;
        public const int GenerateKeyMessageComposer = 257;
        public const int UserRightsMessageComposer = 2;
        public const int jesaispasMessageComposer = 290;
        public const int AuthenticationOkMessageComposer = 3;
        public const int SendNotifyMessageComposer = 139;
        public const int ClubStatusMessageComposer = 7;
        public const int UserInfoMessageComposer = 5;
        public const int RefreshNavigatorMessageComposer = 220;
        public const int PopulateNavigatorMessageComposer = 221;
        public const int GetRecommendedRoomsMessageComposer = 351;
        public const int GetEventsDataMessageComposer = 368;
        public const int CreditBalanceMessageComposer = 6;
        public const int TicketBalanceMessageComposer = 124;
    }
}