﻿using System;
using System.Collections.Generic;
using System.Text;
using PrjTwentySix.Communication.Packets.Outgoing;
using PrjTwentySix.Core.Encoding;

namespace Plus.Communication.Packets.Outgoing
{
    public class ServerPacket : IServerPacket
    {
        private readonly Encoding Encoding = Encoding.Default;

        private StringBuilder Body = new StringBuilder();

        public ServerPacket(int id)
        {
            Id = id;
        }

        public string getId()
        {
            return HabboEncoding.encodeB64(Id, 2);
        }

        public int Id { get; private set; }

        public void WriteString(string body)
        {
            Body.Append(body);
        }

        public void WriteInteger(int i)
        {
            Body.Append(HabboEncoding.encodeVL64(i));
        }

        public void WriteIntegerWithChar(int i)
        {
            Body.Append(HabboEncoding.encodeVL64(i) + Convert.ToChar(2));
        }

        public void WriteStringWithChar(string body)
        {
            Body.Append(body + Convert.ToChar(2));
        }

        public string GetBody()
        {
            return getId() + Body.ToString();
        }
    }
}