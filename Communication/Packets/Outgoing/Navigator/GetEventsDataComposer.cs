﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Navigator
{
    class GetEventsDataComposer : ServerPacket
    {
        public GetEventsDataComposer()
            : base(ServerPacketHeader.GetEventsDataMessageComposer)
        {
            base.WriteInteger(0);
        }
    }
}