﻿using Plus.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Navigator;
using PrjTwentySix.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Navigator
{
    class RefreshNavigatorComposer : ServerPacket
    {
        public RefreshNavigatorComposer(GameClient Session, RoomCategory Category, int HideFullRooms)
            : base(ServerPacketHeader.RefreshNavigatorMessageComposer)
        {
            base.WriteInteger(HideFullRooms);
            base.WriteInteger(Category.Id);
            if (Category.PublicSpaces == 1)
                base.WriteInteger(0);
            else
                base.WriteInteger(2);

            base.WriteStringWithChar(Category.Caption);
            base.WriteInteger(0); //Current Users to do with future RoomData
            base.WriteInteger(0); // max Users to do with future RoomData
            base.WriteInteger(Category.ParentId);            

            List<RoomData> Rooms = RoomFactory.TryGetDataByCategory(Category.Id);

            if (Category.PublicSpaces == 0)
                base.WriteInteger(Rooms.Count);

            foreach (RoomData data in Rooms)
            {
                if (Category.PublicSpaces == 1)
                {
                    base.WriteInteger(data.Id);
                    base.WriteInteger(1);
                    base.WriteStringWithChar(data.Name);
                    base.WriteInteger(data.UsersNow);
                    base.WriteInteger(data.UsersMax);
                    base.WriteInteger(data.CategoryId);
                    base.WriteStringWithChar(data.Description);
                    base.WriteInteger(data.Id);
                    base.WriteInteger(0);
                    base.WriteStringWithChar(data.CCTS);
                    base.WriteInteger(0);
                    base.WriteInteger(1);
                }
                else
                {
                    base.WriteInteger(data.Id);
                    base.WriteStringWithChar(data.Name);

                    if (Session.GetHabbo().Id == data.OwnerId || data.ShowName)
                        base.WriteStringWithChar(data.Owner);
                    else
                        base.WriteStringWithChar("-");

                    switch (data.State)
                    {
                        case RoomAccess.PASSWORD:
                            base.WriteStringWithChar("password");
                            break;

                        case RoomAccess.DOORBELL:
                            base.WriteStringWithChar("closed");
                            break;

                        default:
                            base.WriteStringWithChar("open");
                            break;
                    }

                    base.WriteInteger(data.UsersNow);
                    base.WriteInteger(data.UsersMax);
                    base.WriteStringWithChar(data.Description);
                }
            }

            foreach (RoomCategory SubCategory in PrjTwentySixEnv.GetGame().GetNavigatorManager().GetChildRoomCategories(Category.Id))
            {
                if (Session.GetHabbo().Rank >= SubCategory.MinRankAccess)
                {
                    base.WriteInteger(SubCategory.Id);
                    base.WriteInteger(0);
                    base.WriteStringWithChar(SubCategory.Caption);
                    base.WriteInteger(0); //Current Users to do with future RoomData
                    base.WriteInteger(0); // max Users to do with future RoomData
                    base.WriteInteger(Category.Id);
                }
            }
        }
    }
}