﻿using Plus.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Navigator
{
    class GetRecommendedRoomsComposer : ServerPacket
    {
        public GetRecommendedRoomsComposer(ICollection<RecommendedRoom> RecommendedRooms)
            : base(ServerPacketHeader.GetRecommendedRoomsMessageComposer)
        {
            List<RoomData> Rooms = new List<RoomData>();
            foreach (RecommendedRoom Recommended in RecommendedRooms.ToList())
            {
                if (Recommended == null)
                    continue;

                RoomData Data = null;
                if (!RoomFactory.TryGetData(Recommended.RoomId, out Data))
                    continue;

                if (Data.OwnerId == 0)
                    continue;

                if (!Rooms.Contains(Data))
                    Rooms.Add(Data);
            }

            base.WriteInteger(Rooms.Count);
            foreach (RoomData Room in Rooms)
            {
                base.WriteInteger(Room.Id);
                base.WriteStringWithChar(Room.Name);
                base.WriteStringWithChar(Room.Owner);
                base.WriteStringWithChar(Room.State.ToString());
                base.WriteInteger(Room.UsersNow);
                base.WriteInteger(Room.UsersMax);
                base.WriteStringWithChar(Room.Description);
            }
        }
    }
}