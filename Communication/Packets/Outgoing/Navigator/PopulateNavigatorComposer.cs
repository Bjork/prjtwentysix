﻿using Plus.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using PrjTwentySix.HabboHotel.Navigator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Navigator
{
    class PopulateNavigatorComposer : ServerPacket
    {
        public PopulateNavigatorComposer(List<RoomCategory> Categories)
            : base(ServerPacketHeader.PopulateNavigatorMessageComposer)
        {
            base.WriteInteger(Categories.Count);
            foreach (RoomCategory Category in Categories)
            {
                base.WriteInteger(Category.Id);
                base.WriteStringWithChar(Category.Caption);
            }
        }
    }
}