﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Handshake
{
    class HotelAlertComposer : ServerPacket
    {
        public HotelAlertComposer(string Message)
            : base(ServerPacketHeader.SendNotifyMessageComposer)
        {
            base.WriteString(Message);
        }
    }
}