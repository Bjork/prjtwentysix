﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Inventory.Purse
{
    class TicketBalanceComposer : ServerPacket
    {
        public TicketBalanceComposer(int userTickets)
            : base(ServerPacketHeader.TicketBalanceMessageComposer)
        {
            base.WriteStringWithChar(userTickets + ".0");
        }
    }
}