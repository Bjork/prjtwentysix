﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Inventory.Purse
{
    class CreditBalanceComposer : ServerPacket
    {
        public CreditBalanceComposer(int userCredits)
            : base(ServerPacketHeader.CreditBalanceMessageComposer)
        {
            base.WriteStringWithChar(userCredits + ".0");
        }
    }
}