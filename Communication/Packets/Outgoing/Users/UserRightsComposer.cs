﻿using Plus.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Handshake
{
    class UserRightsComposer : ServerPacket
    {
        public UserRightsComposer(GameClient Session)
            : base(ServerPacketHeader.UserRightsMessageComposer)
        {
            base.WriteString(Session.GetHabbo().GetPermissions().GetAllPermissions());
        }
    }
}