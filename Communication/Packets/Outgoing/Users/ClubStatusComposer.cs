﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Users
{
    class ClubStatusComposer : ServerPacket
    {
        public ClubStatusComposer(int restingDays, int passedMonths, int restingMonths)
            : base(ServerPacketHeader.ClubStatusMessageComposer)
        {
            base.WriteStringWithChar("club_habbo");
            base.WriteInteger(restingDays);
            base.WriteInteger(passedMonths);
            base.WriteInteger(restingMonths);
            base.WriteInteger(1);
        }
    }
}