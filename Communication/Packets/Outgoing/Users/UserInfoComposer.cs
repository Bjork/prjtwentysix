﻿using Plus.Communication.Packets.Outgoing;
using PrjTwentySix.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Handshake
{
    class UserInfoComposer : ServerPacket
    {
        public UserInfoComposer(GameClient Session)
            : base(ServerPacketHeader.UserInfoMessageComposer)
        {
            base.WriteIntegerWithChar(Session.GetHabbo().Id);
            base.WriteStringWithChar(Session.GetHabbo().Username);
            base.WriteStringWithChar(Session.GetHabbo().Look);
            base.WriteStringWithChar(Session.GetHabbo().Gender);
            base.WriteStringWithChar(Session.GetHabbo().Motto);
            base.WriteIntegerWithChar(Session.GetHabbo().Tickets);
            base.WriteStringWithChar(Session.GetHabbo().SwimLook);
            base.WriteIntegerWithChar(Session.GetHabbo().Photos);
        }
    }
}
