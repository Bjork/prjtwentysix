﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing
{
    class GenerateKeyComposer : ServerPacket
    {
        public GenerateKeyComposer()
            : base(ServerPacketHeader.GenerateKeyMessageComposer)
        {
            base.WriteInteger(6);
            base.WriteInteger(0);
            base.WriteInteger(1);
            base.WriteInteger(1);
            base.WriteInteger(1);
            base.WriteInteger(3);
            base.WriteInteger(0);
            base.WriteInteger(2);
            base.WriteInteger(1);
            base.WriteInteger(4);
            base.WriteInteger(1);
            base.WriteInteger(5);
            base.WriteString("dd-MM-yyyy");
        }
    }
}
