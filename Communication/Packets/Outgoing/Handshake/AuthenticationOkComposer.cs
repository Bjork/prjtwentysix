﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing.Handshake
{
    class AuthenticationOkComposer : ServerPacket
    {
        public AuthenticationOkComposer()
            : base(ServerPacketHeader.AuthenticationOkMessageComposer)
        {
        }
    }
}