﻿using Plus.Communication.Packets.Outgoing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets.Outgoing
{
    class InitCryptoComposer : ServerPacket
    {
        public InitCryptoComposer()
            : base(ServerPacketHeader.InitCryptoMessageComposer)
        {
            base.WriteInteger(1);
            base.WriteInteger(0);
        }
    }
}
