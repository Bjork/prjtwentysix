﻿using log4net;
using Plus.Communication.Packets.Incoming;
using PrjTwentySix.Communication.Packets.Incoming;
using PrjTwentySix.Communication.Packets.Incoming.Handshake;
using PrjTwentySix.Communication.Packets.Incoming.Navigator;
using PrjTwentySix.Communication.Packets.Incoming.Users;
using PrjTwentySix.HabboHotel.GameClients;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PrjTwentySix.Communication.Packets
{
    public class PacketManager
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.Communication.Packets");

        /// <summary>
        ///     Testing the Task code
        /// </summary>
        private readonly bool IgnoreTasks = true;

        /// <summary>
        ///     The maximum time a task can run for before it is considered dead
        ///     (can be used for debugging any locking issues with certain areas of code)
        /// </summary>
        private readonly int MaximumRunTimeInSec = 300; // 5 minutes

        /// <summary>
        ///     Should the handler throw errors or log and continue.
        /// </summary>
        private readonly bool ThrowUserErrors = false;

        /// <summary>
        ///     The task factory which is used for running Asynchronous tasks, in this case we use it to execute packets.
        /// </summary>
        private readonly TaskFactory _eventDispatcher;

        private readonly Dictionary<int, IPacketEvent> _incomingPackets;
        private readonly Dictionary<int, string> _packetNames;

        /// <summary>
        ///     Currently running tasks to keep track of what the current load is
        /// </summary>
        private readonly ConcurrentDictionary<int, Task> _runningTasks;

        public PacketManager()
        {
            this._incomingPackets = new Dictionary<int, IPacketEvent>();

            this._eventDispatcher = new TaskFactory(TaskCreationOptions.PreferFairness, TaskContinuationOptions.None);
            this._runningTasks = new ConcurrentDictionary<int, Task>();
            this._packetNames = new Dictionary<int, string>();            

            this.RegisterHandshake();
            this.RegisterUsers();
            this.RegisterNavigator();


            this.RegisterNames();
        }

        public void TryExecutePacket(GameClient Session, ClientPacket Packet)
        {
            IPacketEvent Pak = null;

            if (!_incomingPackets.TryGetValue(Packet.Id, out Pak))
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    log.Debug("Unhandled Packet: " + Packet.ToString());
                return;
            }

            if (System.Diagnostics.Debugger.IsAttached)
            {
                if (_packetNames.ContainsKey(Packet.Id))
                    log.Debug("[INCOMING] Handled Packet: [" + Packet.Id + "] " + _packetNames[Packet.Id] + "  " + Packet.ToString());
                else
                    log.Debug("[INCOMING] Handled Packet: [" + Packet.Id + "] UnnamedPacketEvent " + Packet.ToString());
            }

            Pak.Parse(Session, Packet);
        }

        public void WaitForAllToComplete()
        {
            foreach (Task t in this._runningTasks.Values.ToList())
            {
                t.Wait();
            }
        }

        public void UnregisterAll()
        {
            this._incomingPackets.Clear();
        }

        private void RegisterHandshake()
        {
            this._incomingPackets.Add(ClientPacketHeader.pingReceivedMessageEvent, new PingReceivedEvent());
            this._incomingPackets.Add(ClientPacketHeader.initCryptoEvent, new InitCryptoEvent());
            this._incomingPackets.Add(ClientPacketHeader.packetSSOMessageEvent, new GenerateKeyEvent());
            this._incomingPackets.Add(ClientPacketHeader.initializePlayerMessageEvent, new SSOTicketEvent());
            this._incomingPackets.Add(ClientPacketHeader.pingMessageEvent, new PingEvent());
        }

        private void RegisterUsers()
        {
            this._incomingPackets.Add(ClientPacketHeader.getClubStatusMessageEvent, new GetClubStatusEvent());
            this._incomingPackets.Add(ClientPacketHeader.getUserInfoMessageEvent, new GetUserInfoEvent());
        }

        private void RegisterNavigator()
        {
            this._incomingPackets.Add(ClientPacketHeader.refreshNavigatorMessageEvent, new RefreshNavigatorEvent());
            this._incomingPackets.Add(ClientPacketHeader.populateNavigatorMessageEvent, new PopulateNavigatorEvent());
            this._incomingPackets.Add(ClientPacketHeader.getRecommendedRoomsMessageEvent, new GetRecommendedRoomsEvent());
            this._incomingPackets.Add(ClientPacketHeader.getEventsDataMessageEvent, new GetEventsDataEvent());
        }

        private void RegisterNames()
        {
            this._packetNames.Add(ClientPacketHeader.pingReceivedMessageEvent, "PingReceivedEvent");
            this._packetNames.Add(ClientPacketHeader.initCryptoEvent, "InitCryptoEvent");
            this._packetNames.Add(ClientPacketHeader.packetSSOMessageEvent, "GenerateKeyEvent");
            this._packetNames.Add(ClientPacketHeader.initializePlayerMessageEvent, "SSOTicketEvent");
            this._packetNames.Add(ClientPacketHeader.getClubStatusMessageEvent, "GetClubStatusEvent");
            this._packetNames.Add(ClientPacketHeader.getUserInfoMessageEvent, "GetUserInfoEvent");
            this._packetNames.Add(ClientPacketHeader.refreshNavigatorMessageEvent, "RefreshNavigatorEvent");
            this._packetNames.Add(ClientPacketHeader.populateNavigatorMessageEvent, "PopulateNavigatorEvent");
            this._packetNames.Add(ClientPacketHeader.getRecommendedRoomsMessageEvent, "GetRecommendedRoomsEvent");
            this._packetNames.Add(ClientPacketHeader.pingMessageEvent, "PingEvent");
            this._packetNames.Add(ClientPacketHeader.getEventsDataMessageEvent, "GetEventsDataEvent");
        }
    }
}
